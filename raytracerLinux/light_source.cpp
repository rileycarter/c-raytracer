/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements light_source.h

***********************************************************/

#include <cmath>
#include "light_source.h"

void PointLight::shade( Ray3D& ray, bool feeler ) { // feeler is false if an intersection
	// TODO: implement this function to fill in values for ray.col 
	// using phong shading.  Make sure your vectors are normalized, and
	// clamp colour values to 1.0.
	//
	// It is assumed at this point that the intersection information in ray 
	// is available.  So be sure that traverseScene() is called on the ray 
	// before this function.  

    double ks; // material specular reflection constant
    double kd; // material diffuse reflection constant
    double ka; // material ambient reflection constant
    double alpha = ray.intersection.mat->specular_exp; // material shininess

    float is; // light source specular value
    float id; // light source diffuse value
    float ia; // light source ambient value

    Vector3D L; // direction between the point and light source
    Vector3D N; // the normal at this point
    Vector3D R; // the `perfect` reflection direction
    Vector3D V; // the direction towards the viewer

    float toColour; 

    for (int i = 0; i <= 2; i++)
    {
        ka = ray.intersection.mat->ambient[i];
        ia = _col_ambient[i];
        ray.col[i] = ka * ia;

        if (feeler)
        {
            ks = ray.intersection.mat->specular[i];
            kd = ray.intersection.mat->diffuse[i];

            is = _col_specular[i];
            id = _col_diffuse[i];

            L = _pos - ray.intersection.point;
            L.normalize();
            N = ray.intersection.normal;
            N.normalize();
            R = 2 * ((L.dot(N)) *N) - L;
            R.normalize();
            V = -1 * ray.dir;
            V.normalize();

            ray.col[i] += (kd * std::max(0.0d, L.dot(N) * id ))
                        + (ks * std::max(0.0d, pow(V.dot(R), alpha) * is)); // phong model
        }
    }

    ray.col.clamp();
}

