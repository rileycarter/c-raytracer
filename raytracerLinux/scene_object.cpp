/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		implements scene_object.h

***********************************************************/

#include <cmath>
#include "scene_object.h"
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>

bool UnitSquare::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) {
	// Intersection code for UnitSquare, which is
	// defined on the xy-plane, with vertices (0.5, 0.5, 0), 
	// (-0.5, 0.5, 0), (-0.5, -0.5, 0), (0.5, -0.5, 0), and normal
	// (0, 0, 1).

    Ray3D tRay; // The Ray, to be transformed to object space
    float t;
    float x_check;
    float y_check;
    tRay.origin = worldToModel * ray.origin;
    tRay.dir = worldToModel * ray.dir;

    t = -1 * (tRay.origin[2]) / (tRay.dir[2]); // the intersection
    if (t <= 0)
    {
        return false; // because the intersection shouldn't happen behind our gaze
    }

    x_check = tRay.origin[0] + t * tRay.dir[0]; // x and y coords of intersection
    y_check = tRay.origin[1] + t * tRay.dir[1]; // within the unit square

    if ( std::abs(x_check) <= 0.5 && std::abs(y_check) <= 0.5)
    {
        // An intersection has occured within our unit square...
        //    is it closer than any previous intersection?
        if (ray.intersection.none || t < ray.intersection.t_value)
        {
            ray.intersection.t_value = t;
            ray.intersection.point = modelToWorld * Point3D(x_check, y_check, 0);
            ray.intersection.normal = worldToModel.transpose() * Vector3D(0, 0, 1);
            ray.intersection.normal.normalize();
            ray.intersection.none = false;
            return true;
        }
    }
	return false;
}

bool UnitSphere::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
		const Matrix4x4& modelToWorld ) { 
	// Intersection code for UnitSphere, which is centred 
	// on the origin.  

    Ray3D tRay; // The Ray, to be transformed to object space
    Vector3D tmpOrigin;
    float t;
    float t1; // The parameter where we intersect
    float t2; 
    float a;
    float b;
    float c;
    float delta;
    float x_check;
    float y_check;
    float z_check;
    tRay.origin = worldToModel * ray.origin;
    tRay.dir = worldToModel * ray.dir;
    tRay.dir.normalize();

    // A lazy work around for not having the dot product defined for points
    tmpOrigin = Vector3D(tRay.origin[0], tRay.origin[1], tRay.origin[2]);  

    // Compute delta
    a = tRay.dir.dot(tRay.dir); 
    b = 2 * tRay.dir.dot(tmpOrigin); 
    c = tmpOrigin.dot(tmpOrigin) - 1;

    delta = pow(b, 2) - 4 * a * c;

    if (delta < 0) // No intersection
    {
        return false;
    }
    else if (delta == 0) // One point of intersection
    {
        t = -b / 2 * a;
        if (t <= 0)
        {
            return false; // because the intersection shouldn't happen behind our gaze
        }
    }
    else 
    {
        t1 = (-b - sqrt(delta)) / (2 * a);       
        t2 = (-b + sqrt(delta)) / (2 * a);
        t = std::min(t1, t2);
        if (t <= 0)
        {
            return false; //both intersects are behind our gaze
        }      
    }
    x_check = tRay.origin[0] + t * tRay.dir[0]; // x, y, z coords of intersection
    y_check = tRay.origin[1] + t * tRay.dir[1]; // for the first point
    z_check = tRay.origin[2] + t * tRay.dir[2];

    // Is the intersection closer than any previous intersection?
    if (ray.intersection.none || t < ray.intersection.t_value)
    {
        ray.intersection.t_value = t;
    
        ray.intersection.point = modelToWorld * Point3D(x_check, y_check, z_check);
        ray.intersection.normal = worldToModel.transpose() * Vector3D(x_check, y_check, z_check);
        ray.intersection.normal.normalize();
        ray.intersection.none = false;
        return true;
    }
	return false;
}

// By `unit` we mean radius one, height one, with a center at the origin
bool UnitParaboloid::intersect( Ray3D& ray, const Matrix4x4& worldToModel,
        const Matrix4x4& modelToWorld)
{
    Ray3D tRay;
    Matrix4x4 parabMat;
    float alpha, beta, gamma, delta;
    float t, t1, t2, t3, t4;
    float x_check, y_check, z_check;

    parabMat[0][0] = -1;
    parabMat[1][1] = -1;
    parabMat[3][3] = 1;  

    tRay.origin = worldToModel * ray.origin;
    tRay.dir = worldToModel * ray.dir;

    // Compute the intersections of the ray and the `infinite` part of the cylinder
    alpha = tRay.dir[0] * tRay.dir[0] + tRay.dir[2] * tRay.dir[2];
    beta = 2*(tRay.dir[0]*tRay.origin[0] + tRay.dir[2] * tRay.origin[2]);
    gamma = tRay.origin[0] * tRay.origin[0] + tRay.origin[2] * tRay.origin[2] - 1;

    delta = (beta * beta) - (4 * alpha * gamma);

    if (delta < 0)
    {
        return false;
    }
    else if (delta == 0)
    {
        // There is a unique intersection
        t = -1 * beta / (2 * alpha);
    }
    else
    {
        // There are two intersections.
        t1 = (-1 * beta + sqrt(delta)) / (2 * alpha);
        t2 = (-1 * beta - sqrt(delta)) / (2 * alpha); 
        t = std::min(t1, t2);
        if (t <= 0)
        {
            return false; //both intersects are behind our gaze
        }      
    }

    if (std::abs(tRay.origin[1] + t*tRay.dir[1]) > 0.5)
    {
        // Compute the intersections of the ray and the `bounding planes`,
        t3 = (0.5 - tRay.origin[1]) / tRay.dir[1];
        t4 = (-0.5 - tRay.origin[1]) / tRay.dir[1];

        // These t-values intersect the cap if their x^2 + z^2 < 1
        if (( pow(tRay.origin[0] + t3*tRay.dir[0],2) + pow(tRay.origin[2] + t3*tRay.dir[2],2) > 1) &&
            ( pow(tRay.origin[0] + t4*tRay.dir[0],2) + pow(tRay.origin[2] + t4*tRay.dir[2],2) > 1))
        {
            return false; 
        } 
        else
        {        
            t = std::min(t3, t4);
        }
    } 
    x_check = tRay.origin[0] + t * tRay.dir[0]; // x, y, z coords of intersection
    y_check = tRay.origin[1] + t * tRay.dir[1]; // for the first point
    z_check = tRay.origin[2] + t * tRay.dir[2];

    // Is the intersection closer than any previous intersection?
    if (ray.intersection.none || t < ray.intersection.t_value)
    {
        ray.intersection.t_value = t;
    
        ray.intersection.point = modelToWorld * Point3D(x_check, y_check, z_check);
        if (t == t1 || t == t2)
            { ray.intersection.normal = worldToModel.transpose() * Vector3D(x_check, 0, z_check); }
        else
            { ray.intersection.normal = worldToModel.transpose() * Vector3D(0,1,0); }
        ray.intersection.normal.normalize();
        ray.intersection.none = false;
        return true;
    }
    return false;
} 
