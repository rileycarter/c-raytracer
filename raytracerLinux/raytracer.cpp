/***********************************************************
     Starter code for Assignment 3

     This code was originally written by Jack Wang for
		    CSC418, SPRING 2005

		Implementations of functions in raytracer.h, 
		and the main function which specifies the 
		scene to be rendered.	

***********************************************************/


#include "raytracer.h"
#include "bmp_io.h"
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <time.h>

Raytracer::Raytracer() : _lightSource(NULL) {
	_root = new SceneDagNode();
}

Raytracer::~Raytracer() {
	delete _root;
}

SceneDagNode* Raytracer::addObject( SceneDagNode* parent, 
		SceneObject* obj, Material* mat ) {
	SceneDagNode* node = new SceneDagNode( obj, mat );
	node->parent = parent;
	node->next = NULL;
	node->child = NULL;
	
	// Add the object to the parent's child list, this means
	// whatever transformation applied to the parent will also
	// be applied to the child.
	if (parent->child == NULL) {
		parent->child = node;
	}
	else {
		parent = parent->child;
		while (parent->next != NULL) {
			parent = parent->next;
		}
		parent->next = node;
	}
	
	return node;;
}

LightListNode* Raytracer::addLightSource( LightSource* light ) {
	LightListNode* tmp = _lightSource;
	_lightSource = new LightListNode( light, tmp );
	return _lightSource;
}

void Raytracer::rotate( SceneDagNode* node, char axis, double angle ) {
	Matrix4x4 rotation;
	double toRadian = 2*M_PI/360.0;
	int i;
	
	for (i = 0; i < 2; i++) {
		switch(axis) {
			case 'x':
				rotation[0][0] = 1;
				rotation[1][1] = cos(angle*toRadian);
				rotation[1][2] = -sin(angle*toRadian);
				rotation[2][1] = sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'y':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][2] = sin(angle*toRadian);
				rotation[1][1] = 1;
				rotation[2][0] = -sin(angle*toRadian);
				rotation[2][2] = cos(angle*toRadian);
				rotation[3][3] = 1;
			break;
			case 'z':
				rotation[0][0] = cos(angle*toRadian);
				rotation[0][1] = -sin(angle*toRadian);
				rotation[1][0] = sin(angle*toRadian);
				rotation[1][1] = cos(angle*toRadian);
				rotation[2][2] = 1;
				rotation[3][3] = 1;
			break;
		}
		if (i == 0) {
		    node->trans = node->trans*rotation; 	
			angle = -angle;
		} 
		else {
			node->invtrans = rotation*node->invtrans; 
		}	
	}
}

void Raytracer::translate( SceneDagNode* node, Vector3D trans ) {
	Matrix4x4 translation;
	
	translation[0][3] = trans[0];
	translation[1][3] = trans[1];
	translation[2][3] = trans[2];
	node->trans = node->trans*translation; 	
	translation[0][3] = -trans[0];
	translation[1][3] = -trans[1];
	translation[2][3] = -trans[2];
	node->invtrans = translation*node->invtrans; 
}

void Raytracer::scale( SceneDagNode* node, Point3D origin, double factor[3] ) {
	Matrix4x4 scale;
	
	scale[0][0] = factor[0];
	scale[0][3] = origin[0] - factor[0] * origin[0];
	scale[1][1] = factor[1];
	scale[1][3] = origin[1] - factor[1] * origin[1];
	scale[2][2] = factor[2];
	scale[2][3] = origin[2] - factor[2] * origin[2];
	node->trans = node->trans*scale; 	
	scale[0][0] = 1/factor[0];
	scale[0][3] = origin[0] - 1/factor[0] * origin[0];
	scale[1][1] = 1/factor[1];
	scale[1][3] = origin[1] - 1/factor[1] * origin[1];
	scale[2][2] = 1/factor[2];
	scale[2][3] = origin[2] - 1/factor[2] * origin[2];
	node->invtrans = scale*node->invtrans; 
}

Matrix4x4 Raytracer::initInvViewMatrix( Point3D eye, Vector3D view, 
		Vector3D up ) {
	Matrix4x4 mat; 
	Vector3D w;
	view.normalize();
	up = up - up.dot(view)*view;
	up.normalize();
	w = view.cross(up);

	mat[0][0] = w[0];
	mat[1][0] = w[1];
	mat[2][0] = w[2];
	mat[0][1] = up[0];
	mat[1][1] = up[1];
	mat[2][1] = up[2];
	mat[0][2] = -view[0];
	mat[1][2] = -view[1];
	mat[2][2] = -view[2];
	mat[0][3] = eye[0];
	mat[1][3] = eye[1];
	mat[2][3] = eye[2];

	return mat; 
}

void Raytracer::traverseScene( SceneDagNode* node, Ray3D& ray ) {
	SceneDagNode *childPtr;

	// Applies transformation of the current node to the global
	// transformation matrices.
	_modelToWorld = _modelToWorld*node->trans;
	_worldToModel = node->invtrans*_worldToModel; 
	if (node->obj) {
		// Perform intersection.
		if (node->obj->intersect(ray, _worldToModel, _modelToWorld)) {
			ray.intersection.mat = node->mat;
		}
	}
	// Traverse the children.
	childPtr = node->child;
	while (childPtr != NULL) {
		traverseScene(childPtr, ray);
		childPtr = childPtr->next;
	}

	// Removes transformation of the current node from the global
	// transformation matrices.
	_worldToModel = node->trans*_worldToModel;
	_modelToWorld = _modelToWorld*node->invtrans;
}

void Raytracer::computeShading( Ray3D& ray ) {
	LightListNode* curLight = _lightSource;
    Ray3D shadow_feeler;
    Point3D tmp;

	for (;;) {
		if (curLight == NULL) break;
		// Each lightSource provides its own shading function.
		// Implement shadows here if needed.
        tmp = ray.intersection.point;
        shadow_feeler.dir = curLight->light->get_position() - tmp;
        shadow_feeler.dir.normalize();          
        shadow_feeler.origin = ray.intersection.point + (0.01 * shadow_feeler.dir);

        traverseScene(_root, shadow_feeler); 

		curLight->light->shade(ray, shadow_feeler.intersection.none);
		curLight = curLight->next;
	}
}

void Raytracer::initPixelBuffer() {
	int numbytes = _scrWidth * _scrHeight * sizeof(unsigned char);
	_rbuffer = new unsigned char[numbytes];
	_gbuffer = new unsigned char[numbytes];
	_bbuffer = new unsigned char[numbytes];
	for (int i = 0; i < _scrHeight; i++) {
		for (int j = 0; j < _scrWidth; j++) {
			_rbuffer[i*_scrWidth+j] = 0;
			_gbuffer[i*_scrWidth+j] = 0;
			_bbuffer[i*_scrWidth+j] = 0;
		}
	}
}

void Raytracer::flushPixelBuffer( char *file_name ) {
	bmp_write( file_name, _scrWidth, _scrHeight, _rbuffer, _gbuffer, _bbuffer );
	delete _rbuffer;
	delete _gbuffer;
	delete _bbuffer;
}

Colour Raytracer::shadeRay( Ray3D& ray, int depth ) {
    int MAXDEPTH = 2;
    int GLOSSRAYS = 16;
	Colour col(0.0, 0.0, 0.0); 
	traverseScene(_root, ray); 
    Ray3D reflection_ray;
    Ray3D refraction_ray;
    double refl_damp, rafl_damp, c1, c2;
    double cs1, cs2, eta; 
	
	// Don't bother shading if the ray didn't hit 
	// anything.
	if (!ray.intersection.none) {
		computeShading(ray); 
		col = ray.col;
        if (depth < MAXDEPTH)
        {
            if (ray.intersection.mat->refl && ray.intersection.mat->clearrefl)
            {
                reflection_ray.origin = ray.intersection.point;
                reflection_ray.dir = -2*(ray.dir.dot(ray.intersection.normal))*ray.intersection.normal + ray.dir;
                reflection_ray.dir.normalize();
                //damping factor of exp(-distance) between origin and intersection  
                refl_damp = exp(-(ray.intersection.point-ray.origin).length()*ray.intersection.mat->damp_refl);
                col = col + (refl_damp * shadeRay(reflection_ray, depth + 1));
                col.clamp();
            }
            else if (ray.intersection.mat->refl)
            {
                reflection_ray.origin = ray.intersection.point;
                reflection_ray.dir = -2*(ray.dir.dot(ray.intersection.normal))*ray.intersection.normal + ray.dir;
                reflection_ray.dir.normalize();
                //damping factor of exp(-distance) between origin and intersection  
                refl_damp = exp(-(ray.intersection.point-ray.origin).length()*ray.intersection.mat->damp_refl);
                col = col + (refl_damp * shadeRay(reflection_ray, depth + 1));

                // To do a glossy reflection, compute the tangent plane at the point of intersection,
                // jitter out some points on that surface, and take their average.
                // tangent plane has the basis of u = reflectiondirection cross intersection normal
                // and v = reflectiondirection cross u.
                Vector3D u = reflection_ray.dir.cross(ray.intersection.normal);
                Vector3D v = reflection_ray.dir.cross(u);

                for (int i = 0; i < GLOSSRAYS; i++)
                {
                    Ray3D gRay;
                    double du = rand() / (double) RAND_MAX;
                    double dv = rand() / (double) RAND_MAX;
                    
                    gRay.origin = reflection_ray.origin;
                    gRay.dir = du * u + dv * v + reflection_ray.dir;
                    gRay.dir.normalize();
                    col = col + (refl_damp * shadeRay(gRay, depth + 1));
                }
                col = (1 / (double) GLOSSRAYS) * col;
                col.clamp(); 
            }
            if (ray.refraction_index > 0 && ray.intersection.mat->refraction_index > 0)
            {  
                c1 = ray.refraction_index;
                c2 = ray.intersection.mat->refraction_index;
                eta = c1 / c2;
                cs1 = -ray.dir.dot(ray.intersection.normal);
                cs2 = 1 - eta * eta * (1 - cs1 * cs1);
                if (cs2 < 0)
                {
                    return Colour(0, 0, 0); // TOTAL INTERNAL REFLECTION / eclipse of the heart
                }
    
                refraction_ray.origin = ray.intersection.point;
                refraction_ray.dir = (eta * ray.dir) + ((eta * cs1 - sqrt(cs2)) * ray.intersection.normal);
                refraction_ray.dir.normalize();
                if (ray.refraction_index != 1) //i assume we'll be going through air 
                {
                    refraction_ray.refraction_index = c2;
                }
                else
                {
                    refraction_ray.refraction_index = 1;
                }
                //maybe do refraction based on log distance??
                rafl_damp = exp(-(ray.intersection.point - ray.origin).length()*ray.intersection.mat->damp_trans);
                col = col + (rafl_damp * shadeRay(refraction_ray, depth+1));     
                col.clamp();
            }
        }  
	}

	// You'll want to call shadeRay recursively (with a different ray, 
	// of course) here to implement reflection/refraction effects.  
    col.clamp();

	return col; 
}


void Raytracer::render( int width, int height, Point3D eye, Vector3D view, 
		Vector3D up, double fov, char* fileName ) {
	Matrix4x4 viewToWorld;
	_scrWidth = width;
	_scrHeight = height;
    
	double factor = (double(height)/2)/tan(fov*M_PI/360.0);
    
    Point3D point_aim; // to be the position of a pixel on the focal plane
    Point3D local_jitter; // will be reused as the `new` location of the jittered camera 
    int dof_strength = 16; // the number of `jitters` to blur the image 
    float dof_length = 1; // distance between the image plane and focal plane 
    int AA = 5;
    float adjust_x;
    float adjust_y;

	initPixelBuffer();
	viewToWorld = initInvViewMatrix(eye, view, up);

	// Construct a ray for each pixel.
	for (int i = 0; i < _scrHeight; i++)
    {
		for (int j = 0; j < _scrWidth; j++)
        {   
            Colour cols = Colour(0, 0, 0);    
            for (int m = 0; m < AA; m++) 
            {
                switch ( m )
                {
                    case '0':
                        adjust_x = 0.5;
                        adjust_y = 0.5;
                        break;
                    case '1':
                        adjust_x = 1.0;
                        adjust_y = 0.0;
                        break;
                    case '2':
                        adjust_x = 0.0;
                        adjust_y = 1.0;
                        break;
                    case '3':
                        adjust_x = 1.0;
                        adjust_y = 1.0;
                        break;
                    case '4':    
                        adjust_x = 0.0;
                        adjust_y = 0.0;
                        break;
                }
                // Sets up ray origin and direction in view space, 
                // image plane is at z = -1.
                Point3D origin(0, 0, 0);
                Vector3D imagePlane;
                imagePlane[0] = (-double(width)/2 + adjust_x + j)/factor;
                imagePlane[1] = (-double(height)/2 + adjust_y + i)/factor;
                imagePlane[2] = -1;
                    
                Ray3D ray;
                
                // To get the ray origin in terms of world space, take viewToWorld * origin
                // To get the ray direction in world space, take viewToWorld * imagePlane
                ray.origin = viewToWorld * origin;
                ray.dir = viewToWorld * imagePlane; 
                ray.dir.normalize();
                
                Colour col = shadeRay(ray, 0); 

                // Calculate the point where this ray hits the focal plane
                point_aim = Point3D(dof_length * imagePlane[0],
                                    dof_length * imagePlane[1],
                                    dof_length * imagePlane[2]);
     
                // Now, shoot some random rays

                if (m <= 4)
                { 
                    for (int k = 0; k < dof_strength; k++)
                    {
                        Ray3D jitter_ray;
                        float du = rand()/float(RAND_MAX);
                        float dv = rand()/float(RAND_MAX);
                        local_jitter = Point3D(-0.1 + du/5, -0.1 + dv/5, 0);

                        jitter_ray.origin = viewToWorld * local_jitter; 
                        jitter_ray.dir =  viewToWorld * (point_aim - local_jitter);
                        jitter_ray.dir.normalize();
                        col = col + shadeRay(jitter_ray, 0);
                    }
                    col = (1 /double(dof_strength+1)) * col; 
                }
                
                cols = cols + col;
            }
        cols = (1/double(AA))*cols;
        _rbuffer[i*width+j] = int(cols[0]*255);
        _gbuffer[i*width+j] = int(cols[1]*255);
        _bbuffer[i*width+j] = int(cols[2]*255);
        }
    }
    flushPixelBuffer(fileName);
}

int main(int argc, char* argv[])
{	
	// Build your scene and setup your camera here, by calling 
	// functions from Raytracer.  The code here sets up an example
	// scene and renders it from two different view points, DO NOT
	// change this if you're just implementing part one of the 
	// assignment.  
	Raytracer raytracer;
	int width = 320; 
	int height = 240; 

	if (argc == 3) {
		width = atoi(argv[1]);
		height = atoi(argv[2]);
	}

    //
    srand(time(NULL));

	// Camera parameters.
	Point3D eye(0, 0, 1);
	Vector3D view(0, 0, -1);
	Vector3D up(0, 1, 0);
	double fov = 60;

	// Defines a material for shading.
	Material gold( Colour(0.3, 0.3, 0.3), Colour(0.75164, 0.60648, 0.22648), 
			Colour(0.628281, 0.555802, 0.366065), 
			51.2, true, 0, 1.0, 1.0, true );
	Material jade( Colour(0, 0, 0), Colour(0.54, 0.89, 0.63), 
	               Colour(0.316228, 0.316228, 0.316228), 
			       12.8, false, 1.66, 1, 1.0, true );

    Material emerald(Colour(0.0215, 0.1745, 0.0215), Colour(0.07568, 0.61424, 0.07568),
                     Colour(0.633, 0.727811, 0.633), 76.8, false, 1.66, 1, 1, true);

    Material r(Colour(0,0,0), Colour(0,0,0), Colour(0,0,0), 31, true, 0, 0, 0, false);

    Material glass( Colour(0.0,0.0,0.0), Colour( 0.588235, 0.670588, 0.729412),
            Colour( 0.9, 0.9, 0.9), 100, false, 2.4, 1.0, 0, false );

	// Defines a point light sourcep
	raytracer.addLightSource( new PointLight(Point3D(5, 6,5), 
				Colour(0.9, 0.9, 0.9) ) );

	// Add a unit square into the scene with material mat.
//\	SceneDagNode* sphere = raytracer.addObject( new UnitSphere(), &jade);
  	SceneDagNode* plane = raytracer.addObject( new UnitSquare(), &jade );
//	SceneDagNode* plane2 = raytracer.addObject( new UnitSquare(), &emerald );
    SceneDagNode* smallSphere = raytracer.addObject( new UnitSphere(), &glass);
    SceneDagNode* smallSphere2 = raytracer.addObject(new UnitSphere(), &gold);
    SceneDagNode* smallSphere3 = raytracer.addObject(new UnitSphere(), &r);
    SceneDagNode* smallSphere4 = raytracer.addObject(new UnitSphere(), &jade);
    SceneDagNode* cylinder = raytracer.addObject( new UnitParaboloid(), &gold);
	
	// Apply some transformations to the unit square.
	double factor1[3] = { 1.0, 2.0, 1.0 };
	double factor2[3] = { 32, 32.0, 1 };
    double factor3[3] = { 0.15, 0.15, 0.15 };
    double factor4[3] = { 0.05, 0.05, 0.05 } ;
//	raytracer.translate(sphere, Vector3D(0, 0, -5));	
//	raytracer.rotate(sphere, 'x', -45); 
//	raytracer.rotate(sphere, 'z', 45); 
//	raytracer.scale(sphere, Point3D(0, 0, 0), factor1);
  	raytracer.translate(plane, Vector3D(0, -1.75, 0));	
    raytracer.rotate(plane, 'x', -90 ); 
	raytracer.scale(plane, Point3D(0, 0, 0), factor2);


    raytracer.translate(smallSphere, Vector3D(.310, -.15, 0));
    raytracer.scale(smallSphere, Point3D(0,0,0), factor3);

    raytracer.translate(smallSphere2, Vector3D(-0.310, -.15, 0));
    raytracer.scale(smallSphere2, Point3D(0,0,0), factor3);

    raytracer.translate(smallSphere3, Vector3D(0.0, -0.15, 0));
    raytracer.scale(smallSphere3, Point3D(0,0,0), factor3);

    raytracer.translate(smallSphere4, Vector3D(0.0, 1.5, -7));
    raytracer.scale(smallSphere4, Point3D(0,0,0), factor3);

//	raytracer.translate(plane2, Vector3D(0, 0, -17));	
//	raytracer.scale(plane2, Point3D(0, 0, 0), factor2);

    raytracer.translate(cylinder, Vector3D(0.1, .5 , -.75));
    raytracer.scale(cylinder, Point3D(0,0,0), factor3);
    raytracer.rotate(cylinder, 'x', 45);

	// Render the scene, feel free to make the image smaller for
	// testing purposes.	
    raytracer.render(width, height, eye, view, up, fov, "view1.bmp");
	
	// Render it from a different point of view.
//	Point3D eye2(4, 2, 1);
//	Vector3D view2(-4, -2, -1);
//	raytracer.render(width, height, eye2, view2, up, fov, "view2.bmp");
	
	return 0;
}

